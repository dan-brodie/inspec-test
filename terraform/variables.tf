variable "project_id" {
  description = "The GCP project to use for integration tests"
  type        = string
  default     = "db-dns-01"
}

variable "region" {
  description = "The GCP region to create and test resources in"
  type        = string
  default     = "us-central1"
}

variable "zone" {
  description = "The GCP zone to create resources in"
  type        = string
  default     = "us-central1-a"
}

variable "network_name" {
  description = "Network name prefix"
  default     = "networka"
}

variable "network_tier" {
  description = "Network network_tier"
  default     = "PREMIUM"
}
