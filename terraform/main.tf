provider "google" {
  project = "db-dns-01"
  region  = "us-central1"
  zone    = "us-central1-a"
}

provider "null" {
}

locals {
  subnet_1 = "${var.network_name}-subnet-01"
  subnet_2 = "${var.network_name}-subnet-02"
}

resource "google_compute_address" "static" {
  name         = "nat"
  address_type = "EXTERNAL"
}

module "vpc" {
  source       = "terraform-google-modules/network/google"
  version      = "~> 3.0"
  project_id   = var.project_id
  network_name = var.network_name
  routing_mode = "REGIONAL"

  subnets = [
    {
      subnet_name   = "${local.subnet_1}"
      subnet_ip     = "10.10.10.0/24"
      subnet_region = var.region
    },
    {
      subnet_name           = "${local.subnet_2}"
      subnet_ip             = "10.10.20.0/24"
      subnet_region         = var.region
      subnet_private_access = "true"
      subnet_flow_logs      = "true"
    },
  ]
}

module "instance_template" {
  source               = "terraform-google-modules/vm/google//modules/instance_template"
  version              = "7.1.0"
  source_image         = ""
  source_image_family  = "centos-7"
  source_image_project = "centos-cloud"
  startup_script       = "./boot.sh"
  region               = var.region
  project_id           = var.project_id
  subnetwork           = local.subnet_2
  service_account = {
    email  = "inspec@db-dns-01.iam.gserviceaccount.com"
    scopes = ["cloud-platform"]
  }
}

module "compute_instance" {
  source            = "terraform-google-modules/vm/google//modules/compute_instance"
  version           = "7.1.0"
  region            = var.region
  zone              = var.zone
  subnetwork        = local.subnet_2
  num_instances     = 1
  hostname          = "apache"
  instance_template = module.instance_template.self_link
  access_config = [{
    nat_ip       = google_compute_address.static.address
    network_tier = var.network_tier
  }, ]
}

